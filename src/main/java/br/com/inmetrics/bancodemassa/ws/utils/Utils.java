package br.com.inmetrics.bancodemassa.ws.utils;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

/**
 * @author Josemarsilva
 *
 */
public final class Utils {
	
	
	/**
	 * parserStringTOJsonObj(String) - Convert String into JsonObject 
	 * @param text
	 * @return JsonObject
	 */
	public static JsonObject parserStringTOJsonObj(String text){
		JsonParser parse = new JsonParser();
		JsonObject json = new JsonObject();
		json = parse.parse(text).getAsJsonObject();
		return json;
	}
	
	/**
	 * parserStringTOJsonArray(String) - Convert String into JsonArray 
	 * @param text
	 * @return JsonArray
	 */
	public static JsonArray parserStringTOJsonArray(String text){
		JsonParser parse = new JsonParser();
		JsonArray jsonArray = new JsonArray();
		if (text!=null) {
			if (!text.equals("")) {
				jsonArray = parse.parse(text).getAsJsonArray();
			}
		}
		return jsonArray;
	}
	
	
}
