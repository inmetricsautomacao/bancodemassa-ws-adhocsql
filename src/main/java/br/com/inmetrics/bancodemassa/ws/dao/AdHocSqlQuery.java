package br.com.inmetrics.bancodemassa.ws.dao;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.JdbcTemplate;

import com.google.gson.JsonObject;

import br.com.inmetrics.bancodemassa.ws.utils.Constantes;
import br.com.inmetrics.bancodemassa.ws.utils.Utils;

//@Repository <-- removi aqui
@Configuration // <-- inclui isso aqui
public class AdHocSqlQuery implements DaoGenerico<JsonObject> {
	
	private Logger logger = Logger.getLogger(AdHocSqlQuery.class);
	
	@Autowired JdbcTemplate jdbcTemplate;
	
	@Bean
	@Primary
	@ConfigurationProperties(prefix="spring.datasource")
	public DataSource primaryDataSource() {
		logger.info("AdHocSqlQuery.primaryDataSource()");
	    return DataSourceBuilder.create().build();
	}
	
	
	/*
	 * find(jsonRequest) returns JsonObject response from request
	 * 
	 * @param json Request
	 * @return json Response
	 *  
	 */
	public JsonObject find(JsonObject json) {
		logger.info("AdHocSqlQuery.find (Request): " + json);
		String resultado = jdbcTemplate.queryForObject(String.format("select %s('%s')", Constantes.FX_ADHOCSQL_QUERY, json), String.class);
		JsonObject jsonObj = Utils.parserStringTOJsonObj(resultado);
		logger.info("AdHocSqlQuery.find (Response): " + jsonObj);
		return jsonObj;
	}

}
