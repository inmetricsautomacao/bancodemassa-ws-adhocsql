package br.com.inmetrics.bancodemassa.ws.resource;

import static br.com.inmetrics.bancodemassa.ws.utils.Utils.parserStringTOJsonObj;

import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.JsonObject;

import br.com.inmetrics.bancodemassa.ws.dao.AdHocSqlQuery;
import br.com.inmetrics.bancodemassa.ws.dao.AdHocSqlQueryExecute;

@RestController
public class AdHocSql {
	
	private Logger logger = Logger.getLogger(AdHocSql.class);
	
	@Autowired AdHocSqlQuery adHocSqlQuery;
	@Autowired AdHocSqlQueryExecute adHocSqlQueryExecute;
	
	@RequestMapping(value = "/query/{json:.+}", method = RequestMethod.GET, headers = "Accept=application/json")
	public String buscar(@PathVariable("json") String json){
		logger.info("AdHocSql.buscar(json): "+ json);
		JsonObject jsonObjectAdHocSqlQuery = adHocSqlQuery.find(parserStringTOJsonObj(json));
		JsonObject jsonObjectAdHocSqlQueryExecute = adHocSqlQueryExecute.find(jsonObjectAdHocSqlQuery);
		return jsonObjectAdHocSqlQueryExecute.toString();
		
//		String responsta;
//			responsta = String.format("Json:{ %s } \n Connection 1 :{%s} \n Connection 2 : {%s}", 
//					jsonObjectAdHocSqlQueryExecute.toString(), 
//					getURL(adHocSqlQuery.primaryDataSource().toString()),
//					getURL(adHocSqlQueryExecute.adhocsqlDatasource().toString())
//					);
//		
//		return responsta;
	}
	
	/**
	 * Metodo para extrair a url de uma DataSource
	 * @param fromDataSourceToString
	 * @return URL=JDBC:
	 * @author Aliomar Junior
	 */
//	private String getURL(String fromDataSourceToString) {
//		List<String> lista = Arrays.asList(fromDataSourceToString.split(";"));
//		for (String value : lista) {
//			if(value.trim().startsWith("url=")) {
//				logger.info(value);
//				return value;
//			}
//		}
//		return lista.toString();
//	}
	
}
