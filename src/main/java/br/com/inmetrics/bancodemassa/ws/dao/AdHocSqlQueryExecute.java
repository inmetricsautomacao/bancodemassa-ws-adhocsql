package br.com.inmetrics.bancodemassa.ws.dao;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import br.com.inmetrics.bancodemassa.ws.utils.Utils;


//@Repository <-- removi aqui
@Configuration // <-- inclui isso aqui
public class AdHocSqlQueryExecute implements DaoGenerico<JsonObject> {
	
	private Logger logger = Logger.getLogger(AdHocSqlQueryExecute.class);

	JdbcTemplate jdbcTemplateQueryExecute;

	@Bean(name = "spring.adhocsqlDatasource")
	@ConfigurationProperties(prefix="spring.adhocsql.datasource")
	public DataSource adhocsqlDatasource() {
		logger.info("AdHocSqlQueryExecute.adhocsqlDatasource()");
		//precisamos melhorar aqui...
		jdbcTemplateQueryExecute = new JdbcTemplate(DataSourceBuilder.create().build());
	    return jdbcTemplateQueryExecute.getDataSource();
	}	
	
	/* 
	 * find(jsonRequest) returns JsonObject response from request
	 * 
	 * @param json Request
	 * @return json Response
	 * 
	 */
	public JsonObject find(JsonObject json) {
		logger.info("AdHocSqlQueryExecute.find (Request): " + json);
		String adHocSqlQuery = getSqlResultQuery(json);
		logger.info("AdHocSqlQueryExecute.find (adHocSqlQuery): " + adHocSqlQuery);
		String adHocSqlQueryResultSet = null;
		JsonArray jsonArrayQueryResult = null;
		logger.info("jdbcTemplateQueryExecute : " + jdbcTemplateQueryExecute.getDataSource().toString());
		if (!adHocSqlQuery.equals("")) {
			try {
				adHocSqlQueryResultSet = jdbcTemplateQueryExecute.queryForObject(adHocSqlQuery, String.class);
			} catch (DataAccessException e) {
				return addSqlErrorToAdHocSqlResponse(e.toString(), json);
			}
			logger.info("AdHocSqlQueryExecute.find (adHocSqlQueryResultSet): " + adHocSqlQueryResultSet);
		}
		jsonArrayQueryResult = Utils.parserStringTOJsonArray(adHocSqlQueryResultSet);
		return addQueryResultToAdHocSqlResponse(jsonArrayQueryResult, json);
	}
	
	
	/**
	 * getSqlResultQuery(json) - Return query from nested json {"sql_result":{"query": "<return-this-as-string>"}}
	 * @param json Json expected format {"sql_result":{"query": "<return-this-as-string>"}}
	 * @return String <return-this-as-string>
	 */
	private String getSqlResultQuery(JsonObject json) {
		try {
			return json.getAsJsonObject("sql_result").get("query").getAsString();
		} catch (Exception e) {
			return (new String(""));
		}
	}
	
	
	/*
	 * addQueryResultToAdHocSqlResponse() - Add jsonQueryResult to jsonAdHocSqlResponse
	 * @param jsonQueryResult JSON with Query Result
	 * @param jsonAdHocSqlResponse JSON to be added to
	 * @return JsonObject jsonObject JSON added result
	 */
	private JsonObject addQueryResultToAdHocSqlResponse(JsonArray jsonQueryResult, JsonObject jsonAdHocSqlResponse) {
		if (jsonQueryResult==null) {
			return jsonAdHocSqlResponse;
		} else {
			JsonObject jsonNewAdHocSqlResponse = new JsonObject();
			try {
				jsonNewAdHocSqlResponse.add("sql", jsonAdHocSqlResponse.get("sql"));
				JsonObject jsonSqlResult = (JsonObject) jsonAdHocSqlResponse.get("sql_result");
				jsonSqlResult.add("query_result", jsonQueryResult);
				jsonNewAdHocSqlResponse.add("sql_result", jsonSqlResult);
			} catch (Exception e) {
				logger.error("AdHocSqlQueryExecute.addQueryResultToAdHocSqlResponse (exception): " + e);
				return jsonAdHocSqlResponse;
			}
			logger.info("AdHocSqlQueryExecute.addQueryResultToAdHocSqlResponse: " + jsonNewAdHocSqlResponse);
			return jsonNewAdHocSqlResponse;
		}
	}
	

	/*
	 * addSqlErrorToAdHocSqlResponse() - Add SqlError to jsonAdHocSqlResponse
	 * @param strSqlError SQL Error with exception
	 * @param jsonAdHocSqlResponse JSON to be added to
	 * @return JsonObject jsonObject JSON added result
	 */
	private JsonObject addSqlErrorToAdHocSqlResponse(String strSqlError, JsonObject jsonAdHocSqlResponse) {
		if (strSqlError==null) {
			return jsonAdHocSqlResponse;
		} else {
			JsonObject jsonNewAdHocSqlResponse = new JsonObject();
			try {
				jsonNewAdHocSqlResponse.add("sql", jsonAdHocSqlResponse.get("sql"));
				JsonObject jsonSqlResult = (JsonObject) jsonAdHocSqlResponse.get("sql_result");
				jsonSqlResult.addProperty("status_code", 0);
				jsonSqlResult.addProperty("status_message", "ERROR: SQL Error occurred during query execution! " + strSqlError);
				jsonNewAdHocSqlResponse.add("sql_result", jsonSqlResult);
			} catch (Exception e) {
				logger.error("AdHocSqlQueryExecute.addSqlErrorToAdHocSqlResponse (exception): " + e);
				return jsonAdHocSqlResponse;
			}
			logger.info("AdHocSqlQueryExecute.addSqlErrorToAdHocSqlResponse: " + jsonNewAdHocSqlResponse);
			return jsonNewAdHocSqlResponse;
		}
	}



}
