# README #


## 1. Introdução ##

Este repositório contém o código fonte do componente **bancodemassa-ws-adhocsql** da solução **Bancodemassa - Cantina**. O componente *bancodemassa-ws-adhocsql* é um pacote .war que disponibiliza serviços de *Execução de uma query ad-hoc na base de dados* através de WebService.

### 2. Documentação ###

### 2.1. Diagrama de Caso de Uso (Use Case Diagram) ###

```image-file
./doc/UseCaseDiagram*.jpg
../../bancodemassa-doc/*

```

### 2.2. Diagrama de Implantação (Deploy Diagram) ###

```image-file
./doc/DeployDiagram*.jpg
../../bancodemassa-doc/*
```

### 2.3. Diagrama Modelo Banco de Dados (Database Data Model) ###

* n/a

## 3. Projeto ##

### 3.1. Pré-requisitos ###

* Linguagem de programação: Java
* IDE: Eclipse
* Apache Tomcat v8.0 para Eclipse
* JDK/JRE: 1.8
* Postgresql 9.5+
* Postgresql database schemas ('bmconfig') instalado
* Postgresql database initialization/configuration scripts executados

### 3.2. Guia para Desenvolvimento ###

* Obtenha o código fonte através de um "git clone". Utilize a branch "master" se a branch "develop" não estiver disponível.
* Faça suas alterações, commit e push na branch "develop".


### 3.3. Guia para Configuração ###

Os principais itens da gestão de configuração do 'bancodemassa-ws-adhocsql' são:

a. Em './src/main/resources/application.properties' configure a conexão JDBC ( host, porta, dbname, username e password ) do banco de dados Postgresql nó primário de banco de dados onde estão armazenadas as classes de massa ad-hoc.

```properties
spring.datasource.url=jdbc:postgresql://localhost:5432/bmnode
spring.datasource.username=postgres
spring.datasource.password=postgres
spring.datasource.driver-class-name=org.postgresql.Driver
spring.datasource.platform=postgres
```

b. Ainda no mesmo arquivo, './src/main/resources/application.properties' configure a conexão JDBC ( host, porta, dbname, username e password ) do banco de dados ad-hoc, seja ele qual for ( Oracle, SQLServer, MySQL, Postgresql, etc) que o nó servirá queries ad-hoc. Neste exemplo, para fins didáticos, vamos utilizar o próprio banco de dados 'bmconfig' do Cantina como banco de dados onde vamos executar as queries ad-hoc.

```properties
spring.adhocsql.datasource.url=jdbc:postgresql://localhost:5432/bmconfig
spring.adhocsql.datasource.username=postgres
spring.adhocsql.datasource.password=postgres
spring.adhocsql.datasource.driver-class-name=org.postgresql.Driver
spring.adhocsql.datasource.platform=postgres
```


### 3.4. Guia para Teste ###

a. CT-01-01: Teste técnico do componente stored function do 'bancodemassa-scripts' está respondendo as requisições que serão feitas pelo componente WebService 'bancodemassa-ws-adhocsql'. 

Este componente  responde a uma requisição de solicitação de massa de uma query ad-hoc, retornando o comando SQL que deverá ser executado no banco de dados ad-hoc. O parâmetro de entrada é um Json com especificação de ( ClasseMassa, ContentType, EnvironmentType ). Para simplificar vamos passar um Json vazio e a resposta esperada deve ser um erro de não reconhecimento dos elementos, mas valida que o componente está funcionando.

```sql
\CONNECT bmnode
SELECT fx_node_test_data_adhocsqlquery( '{}' );
-----------------------------------
{"sql_result": {"id": null, "query": null, "status_code": 0, "status_message": "ERROR: Unrecognized "classe_massa": "" }}
```


b. CT-01-02: Teste técnico do componente stored function do 'bancodemassa-scripts' que interage com o componente 'bancodemassa-ws-adhocsql'. Observar quais sao as classes de massa ad-hoc disponíveis e invocar a função que retorna a query SQL associada a classe de massa. Veja que há exemplos que mostram a conexão corrente do adhocsql. O nó (node) não sabe a principio com que banco de dados está conectando, pois as configurações fazem os apontamentos. O nó (node) quando requisitado uma classe de massa, retorna a query SQL correspondente para a execução pelo 'webservice-adhocsql'.

```sql
\CONNECT bmnode
SELECT cm.classe_massa,
       cm.descricao,
       cm.node_tipo_classe_massa_id,
       tcm.tipo_classe_massa,
       cm.json_config_classe_massa
FROM   bm_node_classe_massa  cm
INNER  JOIN bm_node_tipo_classe_massa tcm
ON     tcm.id = cm.node_tipo_classe_massa_id
WHERE  classe_massa LIKE 'AD-HOC-SQL.%' 
ORDER BY cm.classe_massa;

| classe_massa                         | descricao                         | tipo_class | json_config_classe_massa                                                                                                                                                                                                                                       |
+--------------------------------------+-----------------------------------+------------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| AD-HOC-SQL.ORACLE.CURRENT-CONFIG     | Oracle database configuration     | Ad hoc SQL | {"config": {"sql": {"query": "SELECT sys_context('userenv','instance_name') current_database, user AS current_user, TO_CHAR(sysdate,'YYYY-MM-DD HH24:MI:SS') AS current_datetime FROM dual"}, "tipo_classe_massa": "Ad hoc SQL", "tipo_classe_massa_id": 4}}"  |
| AD-HOC-SQL.ORACLE.NO-RESULT          | Oracle database no result         | Ad hoc SQL | {"config": {"sql": {"query": "SELECT * FROM dual WHERE 0 = 1"}, "tipo_classe_massa": "Ad hoc SQL", "tipo_classe_massa_id": 4}}"                                                                                                                                |
| AD-HOC-SQL.POSTGRESQL.CURRENT-CONFIG | PostgreSql database configuration | Ad hoc SQL | {"config": {"sql": {"query": "SELECT array_to_json(array_agg(row)) FROM (SELECT current_catalog AS current_database_name, current_user, now() AS current_datetime) row"}, "tipo_classe_massa": "Ad hoc SQL", "tipo_classe_massa_id": 4}}"                      |
| AD-HOC-SQL.POSTGRESQL.NO-RESULT      | Postgresql database no result     | Ad hoc SQL | {"config": {"sql": {"query": "SELECT array_to_json(array_agg(row)) FROM (SELECT schemaname, tableowner, tablename FROM pg_tables WHERE 0 = 1) row"}, "tipo_classe_massa": "Ad hoc SQL", "tipo_classe_massa_id": 4}}"                                           |
| AD-HOC-SQL.POSTGRESQL.PG-TABLES      | Postgresql database tables        | Ad hoc SQL | {"config": {"sql": {"query": "SELECT array_to_json(array_agg(row)) FROM (SELECT schemaname, tableowner, tablename FROM pg_tables ORDER BY schemaname, tableowner, tablename) row"}, "tipo_classe_massa": "Ad hoc SQL", "tipo_classe_massa_id": 4}}"            |
| AD-HOC-SQL.POSTGRESQL.SQL-ERROR      | Postgresql database SQL Error     | Ad hoc SQL | {"config": {"sql": {"query": "SELECT THIS MUST CAUSE AN ERROR - HAY DE HACER ERRORES"}, "tipo_classe_massa": "Ad hoc SQL", "tipo_classe_massa_id": 4}}"                                                                                                        |
| AD-HOC-SQL.SQLSERVER.CURRENT-CONFIG  | SqlServer database configuration  | Ad hoc SQL | {"config": {"sql": {"query": "SELECT DB_NAME()  AS [current_database_name], SYSTEM_USER as [current_user], GETDATE()  as [current_datetime]"}, "tipo_classe_massa": "Ad hoc SQL", "tipo_classe_massa_id": 4}}"                                                 |

```

```sql
\CONNECT bmnode
SELECT fx_node_test_data_adhocsqlquery( '{ "sql": {"classe_massa": "AD-HOC-SQL.POSTGRESQL.CURRENT-CONFIG"} }' );
---------------------------------------------------------------------------------------------------------------------------
"{"sql": {"classe_massa": "AD-HOC-SQL.POSTGRESQL.CURRENT-CONFIG"}, "sql_result": {"id": 17, "query": "SELECT array_to_json(array_agg(row)) FROM (SELECT current_catalog AS current_database_name, current_user, now() AS current_datetime) row", "status_code": 1 (...)"
```

c. CT-02-01: Teste basico verificar se o WebService 'bancodemassa-ws-adhocsql' está funcionando

Suba o servico 'bancodemassa-ws-adhocsql' ( Eclipse Run >> Run As >> Tomcat v.8 Server ) e verifique se ele está recebendo as solicitações. A resposta esperada para um Json vazio '{}' é uma mensagem de erro que a classe de massa da solicitação não foi reconhecida.

- request:
```browser
http://localhost:8080/bancodemassa-ws-adhocsql/query/{}
```

- response:

```json
{"sql":null,"sql_result":{"id":null,"query":null,"status_code":0,"status_message":"ERROR: Unrecognized \"classe_massa\": \"\" ","query_result":[]}}
```

d. CT-02-02: Teste o funcionamento da classe de massa de um banco de dados PostgreSql configurado. Solicite a query 'AD-HOC-SQL.POSTGRESQL.CURRENT-CONFIG' ao webservice 'bancodemassa-ws-adhocsql'. Esta classe de massa carregada como exemplo de demonstração, busca as configurações do banco de dados PostgreSql conectado ao servico de adhocsql.

- request:
```browser
http://localhost:8080/bancodemassa-ws-adhocsql/query/{"sql": {"classe_massa": "AD-HOC-SQL.POSTGRESQL.CURRENT-CONFIG"}}
```

- response:
```json
{"sql":{"classe_massa":"AD-HOC-SQL.POSTGRESQL.CURRENT-CONFIG"},"sql_result":{"id":17,"query":"SELECT array_to_json(array_agg(row)) FROM (SELECT current_catalog AS current_database_name, current_user, now() AS current_datetime) row","status_code":1,"status_message":"Success","query_result":[{"current_database_name":"bmconfig","current_user":"bancodemassa","current_datetime":"2018-06-05T22:57:12.052163-03:00"}]}}
```

- comentário: observe que a resposta ao webservice-adhocsql traz a query executada e o result-set da query executada

e. CT-02-03: Teste o funcionamento de outras classe de massa de um banco de dados PostgreSql configurado de forma ad-hoc:
   * AD-HOC-SQL.POSTGRESQL.NO-RESULT: Esta classe de massa foi planejada para não retornar nada, para saber como um result-set vazio é apresentado

- request:
```browser
http://localhost:8080/bancodemassa-ws-adhocsql/query/{"sql": {"classe_massa": "AD-HOC-SQL.POSTGRESQL.NO-RESULT"}}
```
- response:
```json
{"sql":{"classe_massa":"AD-HOC-SQL.POSTGRESQL.NO-RESULT"},"sql_result":{"id":18,"query":"SELECT array_to_json(array_agg(row)) FROM (SELECT schemaname, tableowner, tablename FROM pg_tables WHERE 0 = 1) row","status_code":1,"status_message":"Success","query_result":[]}}
```
- comentário: observe que mesmo uma query que produza um resultado (result-set) vazio, ainda sim o retorno é um array. Sempre a estrutura retornada é um array.

f. CT-02-03: Teste o funcionamento de outras classe de massa de um banco de dados PostgreSql configurado de forma ad-hoc:
   * AD-HOC-SQL.POSTGRESQL.PG-TABLES: Esta classe de massa traz todas as tabelas do catálogo do PostgreSql

- request:
```browser
http://localhost:8080/bancodemassa-ws-adhocsql/query/{"sql": {"classe_massa": "AD-HOC-SQL.POSTGRESQL.PG-TABLES"}}
```
- response:
```json
{
  "sql":
    {
      "classe_massa":"AD-HOC-SQL.POSTGRESQL.PG-TABLES"
    },
  "sql_result":
    {
	  "id":16,
      "query":"SELECT array_to_json(array_agg(row)) FROM (SELECT schemaname, tableowner, tablename FROM pg_tables ORDER BY schemaname, tableowner, tablename) row",
      "status_code":1,
      "status_message":"Success",
      "query_result":
      [
	    {"schemaname":"information_schema","tableowner":"rdsadmin","tablename":"sql_features"},
        {"schemaname":"information_schema","tableowner":"rdsadmin","tablename":"sql_implementation_info"},
        {"schemaname":"public","tableowner":"bmcantina","tablename":"bm_config_tipo_ambiente"},
        {"schemaname":"public","tableowner":"bmcantina","tablename":"bm_config_tipo_reserva"}
      ]
    }
}
```
- comentário: observe que mesmo uma query que produza um resultado (result-set) vazio, também é retornada como um array vazio, isto é []

g. CT-02-04: Teste o funcionamento de outras classe de massa de um banco de dados PostgreSql configurado de forma ad-hoc:
   * AD-HOC-SQL.POSTGRESQL.SQL-ERROR: Esta classe de massa foi planejada para provocar um ERRO no SQL

- request:
```browser
http://localhost:8080/bancodemassa-ws-adhocsql/query/{"sql": {"classe_massa": "AD-HOC-SQL.POSTGRESQL.SQL-ERROR"}}
```
- response:

```json
{
  "sql":
  {
    "classe_massa":"AD-HOC-SQL.POSTGRESQL.SQL-ERROR"
  },
  "sql_result":
  {
    "id":21,
    "query":"SELECT THIS MUST CAUSE AN ERROR - HAY DE HACER ERRORES",
    "status_code":0,
    "status_message":"ERROR: SQL Error occurred during query execution! org.springframework.jdbc.BadSqlGrammarException: StatementCallback; bad SQL grammar [SELECT THIS MUST CAUSE AN ERROR - HAY DE HACER ERRORES]; nested exception is org.postgresql.util.PSQLException: ERROR: syntax error at or near \"CAUSE\"\n  Posição: 18"
  }
}
```


### 3.5. Guia para Implantação ###

* Obtenha o último pacote (.war) estável gerado disponível na sub-pasta './dist'.
* Configure os arquivos de parametrizações conforme o seu 
* Copie o pacote .war para o diretório ./webapp de seu servidor de aplicação Tomcat v8


### 3.6. Guia para Demonstração ###

* n/a


## Referências ##

* n/a
